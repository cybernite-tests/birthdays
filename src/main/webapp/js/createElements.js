export function createBdayTable() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/Birthdays/api/bday/bday_humans",
        cache: false,
        dataType: "json",
        success: function (res) {
            res.forEach(createBdayRow)
        },
        error: function (error) {
            console.error(error);
        }
    });
}

export function createAllUsersTable() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/Birthdays/api/bday/all_humans",
        cache: false,
        dataType: "json",
        success: function (res) {
            res.forEach(createTableRow)
        },
        error: function (error) {
            console.error(error);
        }
    });
}

export function createBdayTitle() {
    let dateObj =  new Date();
    document.getElementById("today_title").innerHTML = "Birthdays Today - " + dateObj.getDate() + "/" + (dateObj.getMonth() + 1);

}

function wishHappyBirthday(id) {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/Birthdays/api/bday/bday_wish?id=" + id,
        cache: false,
        success: function (res) {
            console.log(res)
        },
        error: function (error) {
            console.error(error);
        }
    });
}

function createBdayRow(element) {
    $("#bdtable").append(`<tr class="table_row">` +
        `                    <td>${element.name}</td>` +
        `                    <td>${element.birthDay}/${element.birthMonth}</td>` +
        `                    <td><button type="button" class="btn - btn-success" id="btn${element.id}"> Happy Birthday</button></td>`+
        `                 </tr>`);
    $("#bdtable").on("click", `#btn${element.id}`, () => wishHappyBirthday(element.id.toString()))
}

function createTableRow(element) {
    $("#alltable").append(`<tr>` +
        `                    <td>${element.name}</td>` +
        `                    <td>${element.birthDay}/${element.birthMonth}</td>` +
        `                 </tr>`);
}






