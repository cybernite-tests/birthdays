import {createBdayTitle, createAllUsersTable, createBdayTable} from "./createElements.js";

$(document).ready(function () {
    createBdayTitle();
    createBdayTable();
    createAllUsersTable();
})