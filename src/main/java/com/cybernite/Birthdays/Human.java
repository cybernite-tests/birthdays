package com.cybernite.Birthdays;

public class Human {
	private String name;
	private Integer birthDay;
	private Integer birthMonth;
	private Integer id;
	
	public Human(String name, Integer birthDay, Integer birthMonth, Integer id) {
		this.name = name;
		this.birthDay = birthDay;
		this.birthMonth = birthMonth;
		this.id = id;
	}
	
	public Human() {}

	@Override
	public String toString() {
		return "Human [name=" + name + ", birthDay=" + birthDay + ", birthMonth=" + birthMonth + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBirthDay() {
		return birthDay;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setBirthDay(Integer birthDay) {
		this.birthDay = birthDay;
	}

	public Integer getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(Integer birthMonth) {
		this.birthMonth = birthMonth;
	}
}
