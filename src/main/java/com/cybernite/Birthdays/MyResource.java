package com.cybernite.Birthdays;

import java.util.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path(ServicesConstants.BDAY_SERVICE_PATH)
public class MyResource {
	/**
	 * 
	 * @return
	 */
	@GET
	@Path(ServicesConstants.ALL_HUMANS)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllHumans(){
		try {
			HumanRepository myRepo = new HumanRepository();
			return Response.ok().entity(myRepo.getHumans()).build();
		} catch (Exception ex) {
			ex.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	@GET
	@Path(ServicesConstants.BDAY_HUMANS)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBdayHumans(){
		try {
			Integer dayOfMonth =  Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
			Integer month=  Calendar.getInstance().get(Calendar.MONTH) + 1;
			HumanRepository myRepo = new HumanRepository();
			List<Human> bdayHumans = new ArrayList<>();
			for (Human human : myRepo.getHumans()) {
				if(human.getBirthMonth() == month && human.getBirthDay() == dayOfMonth) {
					bdayHumans.add(human);
				}
			}
			
			return Response.ok().entity(bdayHumans).build();
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GET
	@Path(ServicesConstants.BDAY_WISH)
	public Response wishHappyBday(@QueryParam("id") String id) {
		try {
			Integer dayOfMonth =  Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
			Integer month=  Calendar.getInstance().get(Calendar.MONTH) + 1;
			HumanRepository myRepo = new HumanRepository();
			Integer intId = Integer.parseInt(id);
			for(Human human : myRepo.getHumans()) {
				if(intId == human.getId()) {
					if(human.getBirthDay() == dayOfMonth && human.getBirthMonth() == month) {
						System.out.println("Happy birthday " + human.getName());
					}
					break;
				}
			}
			
			return Response.ok().build();
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
}
