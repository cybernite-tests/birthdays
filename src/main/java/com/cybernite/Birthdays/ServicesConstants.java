package com.cybernite.Birthdays;

public final class ServicesConstants {
	public final static String BDAY_SERVICE_PATH = "bday";	
	public final static String ALL_HUMANS = "all_humans";	
	public final static String BDAY_HUMANS = "bday_humans";	
	public final static String BDAY_WISH = "bday_wish";
}
