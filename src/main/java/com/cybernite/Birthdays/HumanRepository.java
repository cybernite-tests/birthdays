package com.cybernite.Birthdays;

import java.util.*;

public class HumanRepository {
	private List<Human> myHumans;
	
	public HumanRepository() {
		myHumans = Arrays.asList(new Human("Moshe", 14, 10, 1), new Human("Sivan", 7, 2, 2),  new Human("Yakov", 29, 5, 3),
				new Human("Rina", 19, 9, 4), new Human("Avi", 1, 1, 5),new Human("David", 20, 10, 6), new Human("Ron", 20, 10, 7),
				new Human("Matan", 20, 10, 8), new Human("Avraham", 24, 10, 9),new Human("yizhak", 24, 10, 10), new Human("sarah", 26, 10,11),
				new Human("rachel", 26, 10,12), new Human("lea", 26, 10,13));
	};
	
	public List<Human> getHumans() {
		return myHumans;
	}
}
